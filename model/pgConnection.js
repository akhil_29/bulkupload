module.exports = {
    ExecuteQuery: function (query, database, callback) {

        let db = app.get(database);
        db.query(query, function (err, result) {
            if (err) {
                callback(err, null);
            } else {
                if (result == undefined) {
                    callback(err, null);
                } else {
                    callback(null, result.rows);
                }
            }
        });
    }

}