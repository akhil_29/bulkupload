module.exports = {
    insert: function (collection, insertJSON, callback) {
        var db = app.get('mongodb');
        var dbCollection = db.collection(collection);
        dbCollection.insertMany(insertJSON, function (err, result) {
            callback(err, result);
        });
    },
    find: function (collection, whereJSON, sort, callback) {
        var db = app.get('mongodb');
        var collection = db.collection(collection);
        collection.find(whereJSON).sort(sort).toArray(function (err, docs) {
            callback(err, docs)
        });
    },
}