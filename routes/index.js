let express = require('express');
let router = express.Router();
let dataController = require('../controller/dataController')

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', {
    title: 'Express'
  });
});

router.get('/getdata', function (req, res, next) {
  dataController.download(req, res);
});

module.exports = router;