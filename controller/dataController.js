let mongoConnection = require('../model/mongoConnection');
let MongoClient = require('mongodb').MongoClient;
let collectionName = 'dumpData';
const fs = require('fs');
module.exports = {
    add: (req, res) => {
        let response = {};
        if (req.files.length != 0) {
            let rawdata = fs.readFileSync(req.files[0].path);
            let _rawData = JSON.parse(rawdata);

            let total = 3500000 //please if change to _rawData.length if your RAM for nodejs is not default one
            let arr = [];
            let i, j, temparray, chunk = 100;
            for (i = 0, j = total; i < j; i += chunk) {
                temparray = _rawData.slice(i, i + chunk);
                insertDataMongo(temparray);
            }

            function insertDataMongo(_rawData) {
                mongoConnection.insert(collectionName, _rawData, function (mongoError, mongoResponse) {
                    if (!mongoError) {
                        response.success = true;
                        response.data = ''
                        res.json(response)
                    }
                })
            }
        } else {
            response.success = false;
            response.data = ''
            res.json(response)
        }
    },
    /* bulk: (req, res) => {
        let rawdata = fs.readFileSync(req.files[0].path);
        let _rawData = JSON.parse(rawdata);
        MongoClient.connect("mongodb://localhost:27017/bigdata", function (err, db) {
            let db = app.get('mongodb');
            let col = db.collection('dumpData');
            let batch = col.initializeUnorderedBulkOp();

            for (i = 0; i < _rawData.length; i++) {
                batch.insert(_rawData[i]);
            }
            batch.execute(function (err, result) {
                console.dir(err);
                console.dir(result);

            });
        });
    }, */
    download: (req, res) => {
        let response = {};
        let _rawData = {};
        mongoConnection.find(collectionName, _rawData, null, function (mongoError, mongoResponse) {
            if (mongoError) {
                response.success = false
                response.data = null;
                res.json(response)
            } else {
                response.success = true;
                response.data = mongoResponse
                res.json(response)
            }
        })
    }
}