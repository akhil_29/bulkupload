let createError = require('http-errors');
let express = require('express');
let path = require('path');
let cookieParser = require('cookie-parser');
let logger = require('morgan');
let cors = require('cors');
let bodyParser = require('body-parser');

let config = require('./config/default.json');
let indexRouter = require('./routes/index');
let usersRouter = require('./routes/users');
let MongoClient = require('mongodb').MongoClient;
let dataController = require('./controller/dataController')
multer = require('multer');

app = express();
// view engine setup
app.use(bodyParser.json());
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({
  extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, __dirname + '/public/uploads/');
  },
  filename: function (req, file, callback) {
    //console.log('filename:' + JSON.stringify(file));
    var tmp_file;
    var extension = file.mimetype;
    extension = extension.split('/');
    extension = extension[1];
    tmp_file = Date.now() + "." + extension;
    callback(null, tmp_file);
  }
});

app.use(multer({
  dest: __dirname + '/public/uploads/',
  storage: storage,
  fileFilter: function (req, file, cb) {
    cb(null, true);
  }
}).any());

app.use(function (req, res, next) {
  if (typeof req.files == "undefined") {
    req.files = [];
  }
  for (var i = 0; i < req.files.length; i++) {
    req[req.files[i]['fieldname']] = {};
    req[req.files[i]['fieldname']] = req.files[i];
  }

  // Pass to next layer of middleware
  next();
});
// mongo connection

MongoClient.connect(config.connectionString.local_mongo, function (err, database) {
  if (!err) {
    app.set('mongodb', database.db('bigdata'));
    console.log("Mongo DB Instance Connected Succesfully.");
    console.log('Welcome to  API');
    app.post('/bigdata', function (req, res, next) {
      dataController.add(req, res);
    });
  } else {
    console.log("Failed to connect Mongo DB Instance." + JSON.stringify(err));
  }
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;